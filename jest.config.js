module.exports = {
    "preset": "react-native",
    "setupFiles": [
        "react-native-gesture-handler/jestSetup"
    ],
    moduleFileExtensions: ["ts", "tsx", "js", "jsx"],
    "transformIgnorePatterns": [
        "node_modules/?!(react-native-gesture-handler|@react-navigation/.*)"
    ], 
    "preset": "react-native",
    "transform": {
        "^.+\\.(js|jsx)$": "<rootDir>/node_modules/babel-jest",
    },
    "moduleNameMapper": {
        ".+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$": "identity-obj-proxy"
    },
    "unmockedModulePathPatterns": [
        "<rootDir>/node_modules/react",
        "<rootDir>/node_modules/react-dom",
        "<rootDir>/node_modules/react-addons-test-utils",
        "<rootDir>/node_modules/fbjs"
    ]
}