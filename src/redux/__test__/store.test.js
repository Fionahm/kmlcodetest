import {rootReducer, initialState} from '../store';

describe('rootReducer tests', () => {
    it('should return initial state', () => {
        expect(rootReducer()).toEqual(initialState)
    })
})