import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../sagas/rootSagas';

export const initialState = {
    isLoggedIn: false,
    isLoading: false,
    username: undefined,
    athletes: [],
    squads: [],
    current_athlete_id: undefined
}

export const rootReducer = (state = initialState, action = {type: "INIT"}) => {
    const {type} = action;
    switch (type) {
        case 'SUBMIT_LOGIN':
            return {...state, isLoading: true}
        case 'LOGIN_SUCCESS':
            return {...state, isLoggedIn: true, username: action.username, isLoading: false}
        case 'LOGIN_ERROR':
            return {...state, isLoading: false}
        case 'UPDATE_ATHLETES': 
            return {...state, athletes: action.athletes}
        case 'UPDATE_SQUADS': 
            return {...state, squads: action.squads}
        case 'SAVE_CURRENT_ATHLETE':
            console.log({...state, current_athlete_id: action.id})
            return {...state, current_athlete_id: action.id}
    }
    return state;
}

const sagaMiddleware = createSagaMiddleware()

export default createStore(rootReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga)