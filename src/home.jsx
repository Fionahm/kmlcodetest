import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {navigationRef} from './main';

const Home = () => {
    const navigateToAthletes = () => {navigationRef.current.navigate('Athletes')};
    const navigateToSquads = () => {navigationRef.current.navigate('Squads')};
    return  <View style={{flex: 1, alignItems: 'center', justifyContent: 'space-around'}} >
                <View  style={{flex: 2}} />
                < MenuButton text={'View Athletes'} onPress={navigateToAthletes} />
                < MenuButton text={'View Squads'} onPress={navigateToSquads} />
                <View  style={{flex: 4}} />
            </View>
}

const MenuButton = ({text, onPress}) => {
   return <View style={styles.buttonContainer} >
    <TouchableOpacity style={styles.buttonStyle} onPress={onPress} >
        <Text style={styles.buttonTextStyle}>{text}</Text>
    </TouchableOpacity>
</View>
}
const styles = StyleSheet.create({
    buttonContainer: {
        flex: 1, 
        alignContent: 'center', 
        alignSelf: 'stretch', 
        justifyContent: 'center', 
        margin: 30
    },
    buttonStyle: {
        height: '100%', 
        alignItems: 'center', 
        justifyContent: 'center', 
        padding: 10,
        borderRadius: 30, 
        backgroundColor: 'darkgrey'
    },
    buttonTextStyle: {
        color: 'white', 
        fontSize: 20 , 
        fontWeight: '600'
    }

})

export default Home;