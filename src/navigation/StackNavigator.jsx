import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../home';
import Login from '../login';
import Athletes from '../athletes';
import Squads from '../squads';
import SquadDetails from '../squad_details';
import AthleteDetails from '../athlete_details';
import {View, Text} from 'react-native';


const Stack = createStackNavigator();

export const StackNavigator = () => {
    return <Stack.Navigator>
      <Stack.Screen name="KML Login" component={Login}/>
      <Stack.Screen name="Home" component={Home}/>
      <Stack.Screen name="Athletes" component={Athletes}/>
      <Stack.Screen name="Squads" component={Squads}/>
      <Stack.Screen name="AthleteDetails" component={AthleteDetails}/>
      <Stack.Screen name="SquadDetails" component={SquadDetails}/>
    </Stack.Navigator>
}