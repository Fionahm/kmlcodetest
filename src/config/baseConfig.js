export const config = {
    baseURL: 'https://kml-tech-test.glitch.me',
    login: '/session',
    athletes: '/athletes',
    squads: '/squads',
}
