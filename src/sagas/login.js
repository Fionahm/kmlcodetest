import {all, call, put, takeLatest} from 'redux-saga/effects';
import axios from 'axios';
import {config} from '../config/baseConfig';
import {loginSuccessAction, postLoginGetAthletesAction, postLoginGetSquadsAction, loginErrorAction} from '../actions/login';
import {navigationRef} from '../main';

const {baseURL, login, athletes, squads} = config;

export function* handleError(error) {
    yield put(loginErrorAction())
}

export function handleNavigation() {
    navigationRef.current.navigate('Home');
}
export function* handleSuccess(username) {
    yield put(loginSuccessAction(username));
    yield call(handleNavigation)
}

export function* makeApiCall(method, url, data) {
    return yield call(axios, {
        method,
        url,
        data,
        headers: {
            'content-type': 'application/json'
        }
      }); 
}


export function* handleLoginPOSTApi(action) {
    yield {username, password} = action;
    return response = yield call(makeApiCall, 'post', baseURL+login,{
        username,
        password
    });
}

export function* getAthletes() {
    const response = yield call(makeApiCall, 'get', baseURL+athletes);
    if (response && response.status === 200 && response.data && response.data.athletes) yield put(postLoginGetAthletesAction(response.data.athletes));
}

export function* getSquads() {
    const response = yield call(makeApiCall, 'get', baseURL+squads);
    if (response && response.status === 200 && response.data && response.data.squads) yield put(postLoginGetSquadsAction(response.data.squads));
}


export function* handleLogin(action) {
    const response = yield call(handleLoginPOSTApi, action);
    response && response.status === 200 ? yield call(handleSuccess, username) : yield call(handleError, response);
}

export function* handlePostLogin() {
   yield all[{
        athletes: yield call(getAthletes),
        squads: yield call(getSquads)
    }];
}

export default function* () {
    yield takeLatest('SUBMIT_LOGIN', handleLogin);
    yield takeLatest('LOGIN_SUCCESS', handlePostLogin)
}
