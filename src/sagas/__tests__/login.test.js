import {expectSaga} from 'redux-saga-test-plan';
import loginSaga, {handleLogin} from '../login';

describe('test login saga', () => {
    it('should takeLatest SUBMIT_LOGIN', () => {
        return expectSaga(loginSaga)
        .take('SUBMIT_LOGIN', handleLogin)
        .silentRun()
    });
    it('should not takeLatest OTHER_ACTION', () => {
        return expectSaga(loginSaga)
        .not.take('OTHER_ACTION', handleLogin)
        .silentRun()
    });
});