import {expectSaga} from 'redux-saga-test-plan';
import rootSaga from '../rootSagas';
import {handleLogin, handleLoginPostApi} from '../login';
import {call} from 'redux-saga/effects';

describe('test root saga', () => {
    it('should takeLatest for SUBMIT_LOGIN', () => {
        return expectSaga(rootSaga, {username: 'test', password: 'password'})
        .provide([call(handleLoginPostApi, 'test', 'password'), {status: 200}])
        .take('SUBMIT_LOGIN', handleLogin)
        .silentRun()
    });
});