/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import { Provider } from 'react-redux';
import store from './redux/store';
import {NavigationContainer} from '@react-navigation/native';
import {StackNavigator} from './navigation/StackNavigator';

export const navigationRef = React.createRef();

const Main = ({}) => {
  return <Provider store={store} >
    <NavigationContainer ref={navigationRef}>
      <StackNavigator />
    </NavigationContainer>
  </Provider>
};

export default Main;