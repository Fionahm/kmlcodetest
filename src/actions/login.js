export const loginAction = (username, password) => ({type: 'SUBMIT_LOGIN', username, password});
export const loginSuccessAction = (username) => ({type: 'LOGIN_SUCCESS', username});
export const loginErrorAction = () => ({type: 'LOGIN_ERROR'});
export const postLoginGetAthletesAction = (athletes) => ({type: 'UPDATE_ATHLETES', athletes});
export const postLoginGetSquadsAction = (squads) => ({type: 'UPDATE_SQUADS', squads});