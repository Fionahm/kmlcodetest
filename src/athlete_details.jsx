import React from 'react';
import {useSelector} from 'react-redux';
import {Image, StyleSheet, Text, View} from 'react-native';

const AthleteDetails = () => {
    const athlete = useSelector(state => {
        const {athletes, squads, current_athlete_id} = state;
        const current_athlete = athletes.find(athlete => athlete.id === current_athlete_id)
        const athlete_squads =  current_athlete.squad_ids.map(id => {
            return squads.find(squad => squad.id === id)
        });
        current_athlete.squads = athlete_squads
        return current_athlete;
    })
    const getSquads = athlete.squads.map(squad => 
            <View style={{alignItems: 'flex-start', justifyContent: 'center', flexDirection: 'row'}}>
                <Text style={{textAlign: 'center', flex: 1, fontSize: 15, fontWeight: '500'}} >{squad.name.charAt(0).toUpperCase() + squad.name.slice(1)}</Text>
            </View>
    )
    return <View key={athlete.id} style={styles.container}>
        <View style={{flex: 1, margin: 20, borderRadius: 30, backgroundColor: 'lightgrey', alignItems: 'center', justifyContent: 'center'}}>
            <View style={{flex: 1, marginTop: 20, justifyContent: 'center'}}>
                <Text style={{textAlign: 'center', fontSize: 20, fontWeight: '700'}}>{athlete.first_name} {athlete.last_name}</Text>
            </View>
            <View style={{flex: 3, justifyContent: 'center'}}>
                <Image style={{height: 200, width: 200}} source={{url: athlete.image.url}}/>
            </View>
            <View style={{flex: 5}}>
                <Text style={{flex: 1}}>
                    <Text style={{fontSize: 15, fontWeight: '500'}}>Athlete:    </Text>
                    <Text style={{fontSize: 15}} >{athlete.first_name} {athlete.last_name}</Text>
                </Text>
                <Text style={{flex: 1, alignContent: "flex-start", justifyContent: 'center', flexDirection: 'row'}}>
                    <Text style={{flex: 1, paddingLeft: 20, fontSize: 15, fontWeight: '500'}}>Username:    </Text>
                    <Text style={{flex: 1, fontSize: 15}} >{athlete.username}</Text>
                </Text>
                <View style={{flex: 1, paddingBottom: 20, alignSelf: 'center', justifyContent: 'center',  alignContent: 'center'}}>
                    <Text style={{fontSize: 18, fontWeight: '600'}}>Squads</Text>
                </View>
                <View style={{flex: 6}}>
                    {getSquads}
                </View>
            </View>
        </View>
    </View>
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
    },
})

export default AthleteDetails;