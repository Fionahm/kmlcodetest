import {create} from 'react-test-renderer';
import React from 'react';
import Home from '../home';

describe('Home Component Tests', () => {
    test('renders correctly', () => {
        const wrapper = create(<Home />);
        expect(wrapper.toJSON()).toMatchSnapshot();
    })
})