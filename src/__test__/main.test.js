import {act, create} from 'react-test-renderer';
import React from 'react';
import Main from '../main';
import { NavigationContainer } from '@react-navigation/native';

jest.mock('react-native/Libraries/Animated/src/NativeAnimatedHelper');


describe('Main Component Tests', () => {
    test('renders correctly', async () => {
        let wrapper;
        act(() => {
            wrapper = create(<Main />);
        });
        await act(async () => { expect(wrapper.toJSON()).toMatchSnapshot(); })
        expect(wrapper.root.findByType(NavigationContainer)).toBeDefined()
    });
});