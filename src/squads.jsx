import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const Squads = () => {
    return <View style={styles.container}>
        <Text style={styles.title}>
            Squads Screen
        </Text>
    </View>
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        color: 'black',
        fontSize: 20
    }
})

export default Squads;