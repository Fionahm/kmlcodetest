import React, {useState} from 'react';
import {StyleSheet, Text, View, TextInput, TouchableOpacity, ActivityIndicator} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import {loginAction} from './actions/login';

const Login = ({}) => {
    const isLoading = useSelector(state => state.isLoading);
    return <View style={styles.container}>
        <LoginContainer />
        {isLoading && 
        <><View style={{position: 'absolute', top: 0, left: 0, bottom: 0, right: 0, opacity: 0.5, backgroundColor: 'black'}} />
        <ActivityIndicator size="large" color="black" /></>}
        <View style={{flex: 2}}/>
    </View>
}

const LoginContainer = ({}) => {
    const [inputUsernameText, setInputUsernameText] = useState("");
    const [inputPasswordText, setInputPasswordText] = useState("");
    const dispatch = useDispatch();
    const submitLogin = () => {
        dispatch(loginAction(inputUsernameText, inputPasswordText));
        setInputUsernameText("");
        setInputPasswordText("");
    };
    const {loginContainerButtonTextStyle, loginContainerButtonStyle,loginContainerStyle} = styles;
    const disabled = inputUsernameText.length < 2 || inputPasswordText.length < 2;
    return <View style={loginContainerStyle}>
                <TextInputContainer inputText={inputUsernameText} setInputText={setInputUsernameText} text="Username" mask={false}/>
                <TextInputContainer inputText={inputPasswordText} setInputText={setInputPasswordText} text="Password" mask={true}/>
                <TouchableOpacity disabled={disabled} style={loginContainerButtonStyle} onPress={submitLogin} >
                    <Text style={loginContainerButtonTextStyle}>Login</Text>
                </TouchableOpacity>
            </View>
    }

const TextInputContainer = ({text, mask, inputText, setInputText}) => {
    const {textInputContainer, textInputContainerTextStyle, textInputContainerInputStyle} = styles;
    const handleTextInput = value => setInputText(value)
   return <View style={textInputContainer}>
        <Text style={textInputContainerTextStyle}>{text}</Text>
        <TextInput value={inputText} onChangeText={handleTextInput} secureTextEntry={mask} placeholder={text} style={textInputContainerInputStyle}/>
    </View>
}    

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignSelf: 'stretch',
    },
    title: {
        color: 'black',
        fontSize: 20
    },
    textInputContainerTextStyle:{
        color: 'black', 
        fontWeight: '600', 
        textAlign: 'center', 
        paddingBottom: 5, 
        marginTop: 5
    },
    textInputContainerInputStyle: {
        padding: 5, 
        height: 60, 
        backgroundColor: 'white', 
        borderWidth: 0.5, 
        borderColor: 'grey', 
        borderRadius: 7
    },
    textInputContainer: {
        flex: 2
    },
    loginContainerButtonTextStyle: {
        color: 'white', 
        fontWeight: '700', 
        textAlign: 'center', 
        paddingBottom: 5, 
        marginTop: 5
    },
    loginContainerButtonStyle: {
        flex: 1, 
        justifyContent: 'center', 
        backgroundColor: 'darkgrey', 
        alignSelf: 'center', 
        width: 200, 
        borderRadius: 7
    },
    loginContainerStyle: {
        flex: 1, 
        justifyContent: 'center', 
        margin: 50
    }

})

export default Login;