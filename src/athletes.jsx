import React from 'react';
import {StyleSheet, Text, View, Image, ScrollView} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import {navigationRef} from './main';
import {saveSelectedAthlete} from './actions/athlete';

const Athletes = () => {
    const athletes = useSelector(state => state.athletes);
    return <View style={styles.container}>
            <AlthetesList athletes={athletes} />
    </View>
}


const AlthetesList = ({athletes}) => {
    const dispatch = useDispatch();
    const displayAthletes = athletes.length > 0 && athletes.map(item => {
        const onPress = (id) => () => { 
            navigationRef.current.navigate('AthleteDetails');
            dispatch(saveSelectedAthlete(id));
        }
        return <View key={item.id} style={{ flex: 1, alignContent: 'center', justifyContent: 'center', height: 140, backgroundColor: 'lightgrey', borderRadius: 30, margin: 10}}>
        <TouchableOpacity onPress={onPress(item.id)} style={{height: 120, width: '100%', flexDirection:'row', alignItems: 'center', justifyContent: 'center',paddingLeft: 20, paddingRight: 20}}>
            <Image resizeMode={'contain'} source={{url: item.image.url}} style={{height: 80, width: 80}}></Image>
            <Text style={{flex: 3,textAlign: 'center',   paddingLeft: 20,  fontSize: 20,}}>{item.first_name} {item.last_name}</Text>
            <Text style={{flex: 1, textAlign: 'left', fontSize: 10}}>Details ...</Text>
        </TouchableOpacity>
    </View>
    })
    return <ScrollView style={{flex: 1, width: '100%'}} contentContainerStyle={{justifyContent: 'center'}}>
        {displayAthletes}
    </ScrollView>
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        color: 'black',
        fontSize: 20
    }
})

export default Athletes;